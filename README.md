BAG Aanmaken
============

Dit PGSQL-script zal de BAG-dump van NLExtract aanvullen met handige gegevens.

Voorbereiding
-------------

Je moet een PostgreSQL database met de PostGIS extensie hebben. Op de Mac kun je gebruik maken van [postgres.app](http://postgresapp.com). Op andere Unix systemen kun je ze installeren via je package manager. Op Debian:

    sudo apt-get update
    sudo apt-get install postgresql postgis

Maak een database `bag` op je PostgreSQL database met de extensie 'postgis':

    psql -c 'CREATE DATABASE bag'
    psql -d bag -c 'CREATE EXTENSION postgis'

Downloaden, importeren en verrijken van de bag-dump
---------------------------------------------------

Onderstaande procedure kan enkele uren in beslag nemen.

    wget http://data.nlextract.nl/bag/postgis/bag-laatst.backup
    pg_restore --no-owner --no-privileges -d bag bag-laatst.backup
    psql -d bag -a -f bag_create_queries.pgsql

Resultaat
---------

Je hebt nu een database met daarin:

* Alle originele bag-tabellen
* De tabel `bagactueel.verblijfsobject_compleet` met daarin alle verblijfsobjecten met hun oppervlakte, gebruiksdoel en adres;
* De tabel `bagactueel.pand_compleet` met daarin de panden met alle adressen en oppervlakten van de verblijfsobjecten in het pand;
* De tabel `bagactueel.wp_gem_prov` met daarin alle woonplaatsen en hun geovlak, maar ook bijbehorende gemeentenaam en provincienaam.

Deze tabellen zijn netjes geo-indexed zodat je ze snel kunt invoegen. In je GIS-applicatie kun je handig filteren op woonplaatsnaam, gemeentenaam en provincienaam.

Optioneel: ongebruikte tabellen verwijderen
-------------------------------------------

Als je wilt kunnen alle andere tabellen (behalve de nieuw aangemaakte tabellen) verwijderd worden, en dan heb je een redelijk compacte BAG-database. Om dat te doen kun je het script `drop_unused_tables.pgsql` gebruiken:

    psql -d bag -a -f drop_unused_tables.pgsql
