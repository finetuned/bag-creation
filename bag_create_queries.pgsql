/*

BAG importer

Dit script verrijkt de geïmporteerde BAG met de gegevens die
nodig zijn om handige analyses te doen. Dit gebeurt door een
aantal tussenstappen te nemen die uiteindelijk resulteren in
de tabellen pand_compleet en verblijfsobject_compleet. De
overige tabellen kunnen vervolgens verwijderd worden.

*/

DROP TABLE IF EXISTS bagactueel.verblijfsobject_compleet CASCADE;
DROP TABLE IF EXISTS bagactueel.pand_compleet;
DROP TABLE IF EXISTS bagactueel.pand_oppervlakte CASCADE;
DROP TABLE IF EXISTS bagactueel.temp_verblijfsobject_oppervlakte;
DROP MATERIALIZED VIEW IF EXISTS bagactueel.pand_adres;
DROP MATERIALIZED VIEW IF EXISTS bagactueel.wp_gem_prov;

UPDATE bagactueel.pand
SET bouwjaar = NULL
WHERE bouwjaar > 2050;

UPDATE bagactueel.verblijfsobject
SET oppervlakteverblijfsobject = NULL
WHERE oppervlakteverblijfsobject IN (1, 9999, 99999, 999999);

/* Maak nieuwe tabel aan voor complete verblijfsobjecten */

CREATE TABLE bagactueel.verblijfsobject_compleet(
    gid serial,
    identificatie numeric(16,0),
    verblijfsobjectstatus bagactueel.verblijfsobjectstatus,
    oppervlakteverblijfsobject numeric(6,0),
    gebruiksdoelverblijfsobject bagactueel.gebruiksdoelverblijfsobject,
    gerelateerdpand numeric(16,0),
    geopunt geometry(PointZ,28992),
    openbareruimtenaam varchar(80),
    huisnummer numeric(5,0),
    huisletter varchar(1),
    huisnummertoevoeging varchar(4),
    postcode varchar(6),
    woonplaatsnaam varchar(80),
    gemeentenaam varchar(80),
    provincienaam varchar(16)
);

/* Zorg ervoor dat er geen dubbelingen kunnen ontstaan door een UNIQUE INDEX op de identificatie te zetten */
CREATE UNIQUE INDEX bagactueel_identificatie ON bagactueel.verblijfsobject_compleet (identificatie);

/* Voeg de verschillende VO tabellen en de adres-tabel samen */
INSERT INTO bagactueel.verblijfsobject_compleet (
    identificatie,
    verblijfsobjectstatus,
    oppervlakteverblijfsobject,
    gebruiksdoelverblijfsobject,
    gerelateerdpand,
    geopunt,
    openbareruimtenaam,
    huisnummer,
    huisletter,
    huisnummertoevoeging,
    postcode,
    woonplaatsnaam,
    gemeentenaam,
    provincienaam
) SELECT
    VO.identificatie,
    VO.verblijfsobjectstatus,
    VO.oppervlakteverblijfsobject,
    VOGD.gebruiksdoelverblijfsobject,
    VOGP.gerelateerdpand,
    VO.geopunt,
    A.openbareruimtenaam,
    A.huisnummer,
    A.huisletter,
    A.huisnummertoevoeging,
    A.postcode,
    A.woonplaatsnaam,
    A.gemeentenaam,
    A.provincienaam
FROM
    bagactueel.verblijfsobjectactueelbestaand VO
LEFT JOIN bagactueel.verblijfsobjectgebruiksdoelactueelbestaand VOGD
    ON VO.identificatie = VOGD.identificatie
LEFT JOIN bagactueel.verblijfsobjectpandactueelbestaand VOGP
    ON VO.identificatie = VOGP.identificatie
LEFT JOIN bagactueel.adres A
    ON VO.identificatie = A.adresseerbaarobject
    AND A.nevenadres = FALSE
ON CONFLICT DO NOTHING;

/* Maak indexen zodat snel op deze kolommen gefilterd kan worden */
CREATE INDEX verblijfsobject_gerelateerdpand ON bagactueel.verblijfsobject_compleet (gerelateerdpand);
CREATE INDEX verblijfsobject_gebruiksdoel ON bagactueel.verblijfsobject_compleet (gebruiksdoelverblijfsobject);
CREATE INDEX verblijfsobject_woonplaats ON bagactueel.verblijfsobject_compleet (woonplaatsnaam);
CREATE INDEX verblijfsobject_gemeente ON bagactueel.verblijfsobject_compleet (gemeentenaam);
CREATE INDEX verblijfsobject_provincie ON bagactueel.verblijfsobject_compleet (provincienaam);
CREATE INDEX verblijfsobject_geopunt ON bagactueel.verblijfsobject_compleet USING GIST(geovlak);

/* In een paar stappen gaan we de oppervlaktes afleiden voor de panden */
CREATE TABLE bagactueel.temp_verblijfsobject_oppervlakte(
    gid integer,
    gerelateerdpand numeric(16,0),
    oppervlakteverblijfsobject numeric(6,0),
    gebruiksdoelverblijfsobject bagactueel.gebruiksdoelverblijfsobject
);

CREATE UNIQUE INDEX temp_verblijfsobject_dubbelingen ON bagactueel.temp_verblijfsobject_oppervlakte (gid);

INSERT INTO bagactueel.temp_verblijfsobject_oppervlakte (gid, gerelateerdpand, oppervlakteverblijfsobject, gebruiksdoelverblijfsobject)
SELECT gid, gerelateerdpand, oppervlakteverblijfsobject, gebruiksdoelverblijfsobject FROM bagactueel.verblijfsobject_compleet
ON CONFLICT DO NOTHING;

CREATE TABLE bagactueel.pand_oppervlakte(
    identificatie numeric(16,0),
    oppervlakte_totaal numeric(6,0),
    oppervlakte_woonfunctie numeric(6,0),
    oppervlakte_bijeenkomstfunctie numeric(6,0),
    oppervlakte_celfunctie numeric(6,0),
    oppervlakte_gezondheidszorgfunctie numeric(6,0),
    oppervlakte_industriefunctie numeric(6,0),
    oppervlakte_kantoorfunctie numeric(6,0),
    oppervlakte_logiesfunctie numeric(6,0),
    oppervlakte_onderwijsfunctie numeric(6,0),
    oppervlakte_sportfunctie numeric(6,0),
    oppervlakte_winkelfunctie numeric(6,0),
    oppervlakte_overige numeric(6,0),
    aantal_totaal numeric(6,0),
    aantal_woonfunctie numeric(6,0),
    aantal_bijeenkomstfunctie numeric(6,0),
    aantal_celfunctie numeric(6,0),
    aantal_gezondheidszorgfunctie numeric(6,0),
    aantal_industriefunctie numeric(6,0),
    aantal_kantoorfunctie numeric(6,0),
    aantal_logiesfunctie numeric(6,0),
    aantal_onderwijsfunctie numeric(6,0),
    aantal_sportfunctie numeric(6,0),
    aantal_winkelfunctie numeric(6,0),
    aantal_overige numeric(6,0)
);

/* INSERT THE TOTALS INTO TEMPORARY HOLDING TABLE */

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_totaal,
    aantal_totaal
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_woonfunctie,
    aantal_woonfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'woonfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_bijeenkomstfunctie,
    aantal_bijeenkomstfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'bijeenkomstfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_celfunctie,
    aantal_celfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'celfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_gezondheidszorgfunctie,
    aantal_gezondheidszorgfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'gezondheidszorgfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_industriefunctie,
    aantal_industriefunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'industriefunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_kantoorfunctie,
    aantal_kantoorfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'kantoorfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_logiesfunctie,
    aantal_logiesfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'logiesfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_onderwijsfunctie,
    aantal_onderwijsfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'onderwijsfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_sportfunctie,
    aantal_sportfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'sportfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_winkelfunctie,
    aantal_winkelfunctie
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'winkelfunctie'
GROUP BY
    gerelateerdpand;

INSERT INTO bagactueel.pand_oppervlakte(
    identificatie,
    oppervlakte_overige,
    aantal_overige
) SELECT
    gerelateerdpand,
    SUM(oppervlakteverblijfsobject),
    COUNT(*)
FROM
    bagactueel.verblijfsobject_compleet
WHERE
    gebruiksdoelverblijfsobject = 'overige gebruiksfunctie'
GROUP BY
    gerelateerdpand;

CREATE INDEX pand_oppervlakte_ident ON bagactueel.pand_oppervlakte (identificatie);

/* CREATE THE TABLE THAT HOLDS THE TOTALS PER PAND */

CREATE MATERIALIZED VIEW bagactueel.pand_oppervlakte_sum AS
SELECT identificatie,
    SUM(oppervlakte_totaal) oppervlakte_totaal,
    SUM(oppervlakte_woonfunctie) oppervlakte_woonfunctie,
    SUM(oppervlakte_bijeenkomstfunctie) oppervlakte_bijeenkomstfunctie,
    SUM(oppervlakte_celfunctie) oppervlakte_celfunctie,
    SUM(oppervlakte_gezondheidszorgfunctie) oppervlakte_gezondheidszorgfunctie,
    SUM(oppervlakte_industriefunctie) oppervlakte_industriefunctie,
    SUM(oppervlakte_kantoorfunctie) oppervlakte_kantoorfunctie,
    SUM(oppervlakte_logiesfunctie) oppervlakte_logiesfunctie,
    SUM(oppervlakte_onderwijsfunctie) oppervlakte_onderwijsfunctie,
    SUM(oppervlakte_sportfunctie) oppervlakte_sportfunctie,
    SUM(oppervlakte_winkelfunctie) oppervlakte_winkelfunctie,
    SUM(oppervlakte_overige) oppervlakte_overige,
    SUM(aantal_totaal) aantal_totaal,
    SUM(aantal_woonfunctie) aantal_woonfunctie,
    SUM(aantal_bijeenkomstfunctie) aantal_bijeenkomstfunctie,
    SUM(aantal_celfunctie) aantal_celfunctie,
    SUM(aantal_gezondheidszorgfunctie) aantal_gezondheidszorgfunctie,
    SUM(aantal_industriefunctie) aantal_industriefunctie,
    SUM(aantal_kantoorfunctie) aantal_kantoorfunctie,
    SUM(aantal_logiesfunctie) aantal_logiesfunctie,
    SUM(aantal_onderwijsfunctie) aantal_onderwijsfunctie,
    SUM(aantal_sportfunctie) aantal_sportfunctie,
    SUM(aantal_winkelfunctie) aantal_winkelfunctie,
    SUM(aantal_overige) aantal_overige
FROM
    bagactueel.pand_oppervlakte
GROUP BY identificatie;

CREATE INDEX pand_oppervlakte_sum_ident ON bagactueel.pand_oppervlakte_sum (identificatie);



/* Verzamel de adressen per verblijfsobject in een tabel */

CREATE MATERIALIZED VIEW bagactueel.pand_adres AS
SELECT V.gerelateerdpand identificatie,
        string_agg(CONCAT(A.openbareruimtenaam,' ',A.huisnummer,A.huisletter,A.huisnummertoevoeging,' ',A.postcode),' ') adres,
        MIN(A.gemeentenaam) gemeentenaam,
        MIN(A.woonplaatsnaam) woonplaatsnaam,
        MIN(A.provincienaam) provincienaam
FROM bagactueel.verblijfsobject_compleet V
LEFT JOIN bagactueel.adres A
    ON V.identificatie = A.adresseerbaarobject
GROUP BY V.gerelateerdpand;

CREATE INDEX pand_adres_index ON bagactueel.pand_adres (identificatie);


/* Construeer de tabel pand_compleet */

CREATE TABLE bagactueel.pand_compleet(
    gid serial,
    identificatie numeric(16,0),
    pandstatus bagactueel.pandstatus,
    bouwjaar numeric(4,0),
    geovlak geometry(PolygonZ,28992),
    gemeentenaam varchar(80),
    woonplaatsnaam varchar(80),
    provincienaam varchar(16),
    adres TEXT,

    OTO_oppervlakte_totaal numeric(6,0),
    OWO_oppervlakte_woonfunctie numeric(6,0),
    OBI_oppervlakte_bijeenkomstfunctie numeric(6,0),
    OCE_oppervlakte_celfunctie numeric(6,0),
    OGZ_oppervlakte_gezondheidszorgfunctie numeric(6,0),
    OIN_oppervlakte_industriefunctie numeric(6,0),
    OKA_oppervlakte_kantoorfunctie numeric(6,0),
    OLO_oppervlakte_logiesfunctie numeric(6,0),
    OOW_oppervlakte_onderwijsfunctie numeric(6,0),
    OSP_oppervlakte_sportfunctie numeric(6,0),
    OWI_oppervlakte_winkelfunctie numeric(6,0),
    OOV_oppervlakte_overige numeric(6,0),

    ATO_aantal_totaal numeric(6,0),
    AWO_aantal_woonfunctie numeric(6,0),
    ABI_aantal_bijeenkomstfunctie numeric(6,0),
    ACE_aantal_celfunctie numeric(6,0),
    AGZ_aantal_gezondheidszorgfunctie numeric(6,0),
    AIN_aantal_industriefunctie numeric(6,0),
    AKA_aantal_kantoorfunctie numeric(6,0),
    ALO_aantal_logiesfunctie numeric(6,0),
    AOW_aantal_onderwijsfunctie numeric(6,0),
    ASP_aantal_sportfunctie numeric(6,0),
    AWI_aantal_winkelfunctie numeric(6,0),
    AOV_aantal_overige numeric(6,0),

    HFO_hoofdfunctie_naar_oppervlakte varchar(30),
    HFA_hoofdfunctie_naar_aantal varchar(30)
);

INSERT INTO bagactueel.pand_compleet(
    identificatie,
    pandstatus,
    bouwjaar,
    geovlak,
    gemeentenaam,
    woonplaatsnaam,
    provincienaam,
    adres,

    OTO_oppervlakte_totaal,
    OWO_oppervlakte_woonfunctie,
    OBI_oppervlakte_bijeenkomstfunctie,
    OCE_oppervlakte_celfunctie,
    OGZ_oppervlakte_gezondheidszorgfunctie,
    OIN_oppervlakte_industriefunctie,
    OKA_oppervlakte_kantoorfunctie,
    OLO_oppervlakte_logiesfunctie,
    OOW_oppervlakte_onderwijsfunctie,
    OSP_oppervlakte_sportfunctie,
    OWI_oppervlakte_winkelfunctie,
    OOV_oppervlakte_overige,

    ATO_aantal_totaal,
    AWO_aantal_woonfunctie,
    ABI_aantal_bijeenkomstfunctie,
    ACE_aantal_celfunctie,
    AGZ_aantal_gezondheidszorgfunctie,
    AIN_aantal_industriefunctie,
    AKA_aantal_kantoorfunctie,
    ALO_aantal_logiesfunctie,
    AOW_aantal_onderwijsfunctie,
    ASP_aantal_sportfunctie,
    AWI_aantal_winkelfunctie,
    AOV_aantal_overige,

    HFO_hoofdfunctie_naar_oppervlakte,
    HFA_hoofdfunctie_naar_aantal

) SELECT
    P.identificatie,
    P.pandstatus,
    P.bouwjaar,
    P.geovlak,

    A.gemeentenaam,
    A.woonplaatsnaam,
    A.provincienaam,
    A.adres,

    oppervlakte_totaal,
    oppervlakte_woonfunctie,
    oppervlakte_bijeenkomstfunctie,
    oppervlakte_celfunctie,
    oppervlakte_gezondheidszorgfunctie,
    oppervlakte_industriefunctie,
    oppervlakte_kantoorfunctie,
    oppervlakte_logiesfunctie,
    oppervlakte_onderwijsfunctie,
    oppervlakte_sportfunctie,
    oppervlakte_winkelfunctie,
    oppervlakte_overige,

    aantal_totaal,
    aantal_woonfunctie,
    aantal_bijeenkomstfunctie,
    aantal_celfunctie,
    aantal_gezondheidszorgfunctie,
    aantal_industriefunctie,
    aantal_kantoorfunctie,
    aantal_logiesfunctie,
    aantal_onderwijsfunctie,
    aantal_sportfunctie,
    aantal_winkelfunctie,
    aantal_overige,

    CASE WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_woonfunctie THEN 'woonfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_bijeenkomstfunctie THEN 'bijeenkomstfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_celfunctie THEN 'celfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_gezondheidszorgfunctie THEN 'gezondheidszorgfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_industriefunctie THEN 'industriefunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_kantoorfunctie THEN 'kantoorfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_logiesfunctie THEN 'logiesfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_onderwijsfunctie THEN 'onderwijsfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_sportfunctie THEN 'sportfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_winkelfunctie THEN 'winkelfunctie'
        WHEN GREATEST(oppervlakte_woonfunctie, oppervlakte_bijeenkomstfunctie, oppervlakte_celfunctie, oppervlakte_gezondheidszorgfunctie, oppervlakte_industriefunctie, oppervlakte_kantoorfunctie, oppervlakte_logiesfunctie, oppervlakte_onderwijsfunctie, oppervlakte_sportfunctie, oppervlakte_winkelfunctie, oppervlakte_overige) = oppervlakte_overige THEN 'overige gebruiksfunctie'
        ELSE NULL
    END,

    CASE WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_woonfunctie THEN 'woonfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_bijeenkomstfunctie THEN 'bijeenkomstfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_celfunctie THEN 'celfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_gezondheidszorgfunctie THEN 'gezondheidszorgfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_industriefunctie THEN 'industriefunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_kantoorfunctie THEN 'kantoorfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_logiesfunctie THEN 'logiesfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_onderwijsfunctie THEN 'onderwijsfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_sportfunctie THEN 'sportfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_winkelfunctie THEN 'winkelfunctie'
        WHEN GREATEST(aantal_woonfunctie, aantal_bijeenkomstfunctie, aantal_celfunctie, aantal_gezondheidszorgfunctie, aantal_industriefunctie, aantal_kantoorfunctie, aantal_logiesfunctie, aantal_onderwijsfunctie, aantal_sportfunctie, aantal_winkelfunctie, aantal_overige) = aantal_overige THEN 'overige gebruiksfunctie'
        ELSE NULL
    END

FROM bagactueel.pandactueelbestaand P
LEFT JOIN bagactueel.pand_adres A
    ON P.identificatie = A.identificatie
LEFT JOIN bagactueel.pand_oppervlakte_sum PO
    ON P.identificatie = PO.identificatie;

/* Maak indexen op de pand_compleet tabel om opzoeken sneller te maken */

CREATE INDEX pand_compleet_gemeentenaam ON bagactueel.pand_compleet (gemeentenaam);
CREATE INDEX pand_compleet_woonplaatsnaam ON bagactueel.pand_compleet (woonplaatsnaam);
CREATE INDEX pand_compleet_provincienaam ON bagactueel.pand_compleet (provincienaam);
CREATE INDEX pand_compleet_geovlak ON bagactueel.pand_compleet USING GIST(geovlak);

/* Maak tabel met woonplaats-geovlak en bijbehorende woonplaatsnaam, gemeentenaam en provincienaam */

CREATE MATERIALIZED VIEW bagactueel.wp_gem_prov AS
SELECT bagactueel.woonplaatsactueelbestaand.gid, bagactueel.provincie.provincienaam, bagactueel.gemeente.gemeentenaam, bagactueel.woonplaatsactueelbestaand.woonplaatsnaam, bagactueel.woonplaatsactueelbestaand.geovlak
FROM bagactueel.provincie_gemeenteactueelbestaand
JOIN bagactueel.gemeente_woonplaatsactueelbestaand
ON bagactueel.provincie_gemeenteactueelbestaand.gemeentecode = bagactueel.gemeente_woonplaatsactueelbestaand.gemeentecode
JOIN bagactueel.provincie ON bagactueel.provincie_gemeenteactueelbestaand.provinciecode = bagactueel.provincie.provinciecode
JOIN bagactueel.gemeente ON bagactueel.provincie_gemeenteactueelbestaand.gemeentecode = bagactueel.gemeente.gemeentecode
JOIN bagactueel.woonplaatsactueelbestaand ON
bagactueel.gemeente_woonplaatsactueelbestaand.woonplaatscode = bagactueel.woonplaatsactueelbestaand.identificatie;

CREATE INDEX wp_gem_prov_geovlak ON bagactueel.wp_gem_prov USING GIST(geovlak);

/* Voeg woonplaats, gemeente en provincie toe aan alle ~3908287 ongelabelde panden */

UPDATE bagactueel.pand_compleet P
SET woonplaatsnaam = WGP.woonplaatsnaam,
gemeentenaam = WGP.gemeentenaam,
provincienaam = WGP.provincienaam
FROM bagactueel.wp_gem_prov WGP
WHERE ST_Intersects(WGP.geovlak, P.geovlak)
AND P.woonplaatsnaam IS NULL;

/* Ruim ongebruikte tabellen op */

DROP MATERIALIZED VIEW bagactueel.pand_adres;
DROP MATERIALIZED VIEW bagactueel.pand_oppervlakte_sum;
DROP TABLE bagactueel.pand_oppervlakte;
DROP TABLE bagactueel.temp_verblijfsobject_oppervlakte;