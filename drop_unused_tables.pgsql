/* Drop all tables that are not the handy ones we created */

DROP VIEW IF EXISTS bagactueel.adresseerbaarobjectnevenadresactueelbestaand;
DROP VIEW IF EXISTS bagactueel.adresseerbaarobjectnevenadresactueel;
DROP VIEW IF EXISTS bagactueel.gemeente_woonplaatsactueelbestaand;
DROP VIEW IF EXISTS bagactueel.ligplaatsactueelbestaand;
DROP VIEW IF EXISTS bagactueel.ligplaatsactueel;
DROP VIEW IF EXISTS bagactueel.nummeraanduidingactueelbestaand;
DROP VIEW IF EXISTS bagactueel.nummeraanduidingactueel;
DROP VIEW IF EXISTS bagactueel.openbareruimteactueelbestaand;
DROP VIEW IF EXISTS bagactueel.openbareruimteactueel;
DROP VIEW IF EXISTS bagactueel.pandactueelbestaand;
DROP VIEW IF EXISTS bagactueel.pandactueel;
DROP VIEW IF EXISTS bagactueel.provincie_gemeenteactueelbestaand;
DROP VIEW IF EXISTS bagactueel.standplaatsactueelbestaand;
DROP VIEW IF EXISTS bagactueel.standplaatsactueel;
DROP VIEW IF EXISTS bagactueel.verblijfsobjectactueelbestaand;
DROP VIEW IF EXISTS bagactueel.verblijfsobjectactueel;
DROP VIEW IF EXISTS bagactueel.verblijfsobjectgebruiksdoelactueelbestaand;
DROP VIEW IF EXISTS bagactueel.verblijfsobjectgebruiksdoelactueel;
DROP VIEW IF EXISTS bagactueel.verblijfsobjectpandactueelbestaand;
DROP VIEW IF EXISTS bagactueel.verblijfsobjectpandactueel;
DROP VIEW IF EXISTS bagactueel.woonplaatsactueelbestaand;
DROP VIEW IF EXISTS bagactueel.woonplaatsactueel;

DROP TABLE IF EXISTS bagactueel.adres;
DROP TABLE IF EXISTS bagactueel.adresseerbaarobjectnevenadres;
DROP TABLE IF EXISTS bagactueel.gemeente;
DROP TABLE IF EXISTS bagactueel.gemeente_woonplaats;
DROP TABLE IF EXISTS bagactueel.gt_pk_metadata;
DROP TABLE IF EXISTS bagactueel.ligplaats;
DROP TABLE IF EXISTS bagactueel.nlx_bag_info;
DROP TABLE IF EXISTS bagactueel.nlx_bag_log;
DROP TABLE IF EXISTS bagactueel.nummeraanduiding;
DROP TABLE IF EXISTS bagactueel.openbareruimte;
DROP TABLE IF EXISTS bagactueel.pand;
DROP TABLE IF EXISTS bagactueel.pand_oppervlakte;
DROP TABLE IF EXISTS bagactueel.provincie;
DROP TABLE IF EXISTS bagactueel.provincie_gemeente;
DROP TABLE IF EXISTS bagactueel.standplaats;
DROP TABLE IF EXISTS bagactueel.temp_verblijfsobject_oppervlakte;
DROP TABLE IF EXISTS bagactueel.verblijfsobject;
DROP TABLE IF EXISTS bagactueel.verblijfsobjectgebruiksdoel;
DROP TABLE IF EXISTS bagactueel.verblijfsobjectpand;
DROP TABLE IF EXISTS bagactueel.woonplaats;

DROP MATERIALIZED VIEW IF EXISTS bagactueel.pand_adres;
DROP MATERIALIZED VIEW IF EXISTS bagactueel.pand_oppervlakte_sum;